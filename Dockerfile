FROM registry.gitlab.com/digimountain/docker/java:openjdk7

ADD https://repo1.maven.org/maven2/org/eclipse/jetty/jetty-distribution/7.6.21.v20160908/jetty-distribution-7.6.21.v20160908.tar.gz /opt/

RUN cd /opt && mv jetty-* jetty.tgz && tar -zxvf jetty.tgz && rm jetty.tgz && mv jetty-* jetty

WORKDIR /opt/jetty
CMD java -jar start.jar
